﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpLib
{
    public interface IHttp
    {
        Task<Result> SendPost(string url, HttpContent content);
        Task<byte[]> GetByte(string url);

    }
}

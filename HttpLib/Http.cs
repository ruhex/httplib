﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpLib
{
    public class Http : IHttp
    {
        private static Http _instance;
        static readonly HttpClient Client = new HttpClient();

        public static Http GetInstance()
        {
            if (_instance == null)
                _instance = new Http();
            return _instance;
        }

        public async Task<Result> SendPost(string url, HttpContent content)
        {
            try
            {
                using (HttpResponseMessage responseMessage = await Client.PostAsync(url, content))
                {
                    try
                    {
                        return new Result { Status = true, Answer = await responseMessage.Content.ReadAsStringAsync() };
                    }
                    catch (Exception e)
                    {
                        return new Result { Status = false, Answer = e.Message };
                    }
                }
            }
            catch (Exception e)
            {
                return new Result { Status = false, Answer = e.Message };
            }
        }

        public async Task<byte[]> GetByte(string url)
        {
            try
            {
                using (HttpResponseMessage responseMessage = await Client.GetAsync(url))
                {

                    try
                    {
                        return await responseMessage.Content.ReadAsByteArrayAsync();
                    }
                    finally
                    {

                        GC.SuppressFinalize(responseMessage);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HttpLib
{
    public struct Result
    {
        public bool Status;
        public string Answer;
    }
}
